package actors

import java.util.concurrent.TimeUnit

import akka.actor.{Actor, ActorRef, ActorSystem, Props, _}
import akka.pattern.ask
import akka.util.Timeout
import utils.{Audio, Res}

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}
import scala.language.postfixOps

/**
  * Created by Andrea on 07/04/2017.
  */
class MarioActor extends Actor {
  private val GET_COIN_COMMAND: String = "AddCoin"
  private val POWER_UP_COMMAND: String = "PowerUp"
  private val POWER_DOWN_COMMAND: String = "PowerDown"
  private val RESET_COMMAND: String = "Reset"

  private var lifeCounter = 0

  private val system = ActorSystem ("PiecesCounterActor")
  private val piecesCounterActor: ActorRef = system.actorOf (Props [PieceCounterActor], name = "Counter")

  def receive: PartialFunction[Any, Unit] = {
    case GET_COIN_COMMAND =>
      implicit val timeout = Timeout (Duration (5, TimeUnit.MILLISECONDS))
      val future: Future[Int] = ask (piecesCounterActor, GET_COIN_COMMAND).mapTo [Int]
      val result = Await.result (future, timeout.duration)
      println (result + " coin")

      if (result == 7) {
        lifeCounter = lifeCounter + 1
        println (lifeCounter + " extra-life")
        Audio.playSound (Res.AUDIO_POWERUP)
        piecesCounterActor ! RESET_COMMAND
      }

    case POWER_UP_COMMAND =>
      lifeCounter = lifeCounter + 1
      println (lifeCounter + " extra-life")
      Audio.playSound (Res.AUDIO_POWERUP)

    case POWER_DOWN_COMMAND =>
      if (lifeCounter > 0) {
        lifeCounter = lifeCounter - 1
        Audio.playSound (Res.AUDIO_POWERDOWN)
        sender ! true
      } else {
        sender ! false
      }
  }
}
