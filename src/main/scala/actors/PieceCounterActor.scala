package actors

import akka.actor.Actor

/**
  * Created by Andrea on 07/04/2017.
  */
class PieceCounterActor extends Actor {
  private val GET_COIN_COMMAND: String = "AddCoin"
  private val RESET_COMMAND: String = "Reset"

  private var counterCoin = 0

  def receive: PartialFunction[Any, Unit] = {
    case GET_COIN_COMMAND =>
      counterCoin = counterCoin + 1
      sender ! counterCoin

    case RESET_COMMAND => counterCoin = 0
  }
}
