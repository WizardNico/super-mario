package utils

import java.util.logging.{Level, Logger}
import javax.sound.sampled.{AudioInputStream, AudioSystem, Clip}

import scala.beans.BeanProperty

/**
  * Created by Andrea on 01/04/2017.
  */
class AudioDefinition(son: String) {
  @BeanProperty var clip: Clip = _

  private val LOGGER: Logger = Utils.getLogger(classOf[AudioDefinition].getName)

  try {
    val audio: AudioInputStream = AudioSystem.getAudioInputStream(getClass.getResource(son))
    clip = AudioSystem.getClip
    clip.open(audio)
  } catch {
    case e: Exception => LOGGER.log(Level.WARNING, "Something goes wrong with audio execution", e)
  }

  def play(): Unit = {
    clip.start()
  }

  def stop(): Unit = {
    clip.stop()
  }

}
