package utils

import java.awt.event.{KeyEvent, KeyListener}

import controllers.SceneController

/**
  * Created by Andrea on 01/04/2017.
  */
class Keyboard extends KeyListener {

  override def keyPressed(e: KeyEvent): Unit = {
    if (SceneController.getScene.getMario.isAlive) {
      if (e.getKeyCode == KeyEvent.VK_RIGHT) {
        leftMovement()
      } else if (e.getKeyCode == KeyEvent.VK_LEFT) {
        rightMovement()
      }
      if (e.getKeyCode == KeyEvent.VK_UP) {
        jump()
      }
    }
  }

  private def checkPositionOnRight(): Unit = {
    if (SceneController.getScene.getXPos == 4601) {
      SceneController.getScene.setXPos(4600)
      fixBackgroundObjects()
    }
  }

  private def checkPositionOnLeft(): Unit = {
    if (SceneController.getScene.getXPos == -1) {
      SceneController.getScene.setXPos(0)
      fixBackgroundObjects()
    }
  }

  private def fixBackgroundObjects(): Unit = {
    SceneController.getScene.setBackground1PosX(-50)
    SceneController.getScene.setBackground2PosX(750)
  }

  private def jump(): Unit = {
    SceneController.getScene.getMario.setJumping(true)
    Audio.playSound(Res.AUDIO_JUMP)
  }

  private def rightMovement(): Unit = {
    checkPositionOnRight()
    SceneController.getScene.getMario.setMoving(true)
    SceneController.getScene.getMario.setToRight(false)
    SceneController.getScene.setMov(-1)
  }

  private def leftMovement(): Unit = {
    checkPositionOnLeft()
    SceneController.getScene.getMario.setMoving(true)
    SceneController.getScene.getMario.setToRight(true)
    SceneController.getScene.setMov(1)
  }

  override def keyReleased(e: KeyEvent): Unit = {
    SceneController.getScene.getMario.setMoving(false)
    SceneController.getScene.setMov(0)
  }

  override def keyTyped(e: KeyEvent): Unit = {}

}

