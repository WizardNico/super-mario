package utils

/**
  * Created by Andrea on 01/04/2017.
  */
object Audio {
  def playSound(son: String): Unit = {
    val s: AudioDefinition = new AudioDefinition(son)
    s.play()
  }
}

