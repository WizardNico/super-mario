package utils

import java.awt.Image
import java.net.URL
import java.util.logging.Logger
import javax.swing.ImageIcon

import sun.plugin.com.Utils


/**
  * Created by Andrea on 01/04/2017.
  */
object Utils {

  private def getResource(path: String): URL = classOf[Utils].getClass.getResource(path)

  def getImage(path: String): Image = new ImageIcon(getResource(path)).getImage

  def getLogger(className: String): Logger = Logger.getLogger(className)

}
