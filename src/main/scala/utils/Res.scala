package utils

/**
  * Created by Andrea on 01/04/2017.
  */
object Res {

  val IMG_BASE: String = "/resources/imgs/"

  private val AUDIO_BASE: String = "/resources/audio/"

  val IMG_EXT: String = ".png"

  val IMGP_STATUS_NORMAL: String = ""

  val IMGP_STATUS_ACTIVE: String = "A"

  private val IMGP_STATUS_DEAD: String = "E"

  private val IMGP_STATUS_IDLE: String = "F"

  private val IMGP_STATUS_SUPER: String = "S"

  val IMGP_DIRECTION_SX: String = "G"

  val IMGP_DIRECTION_DX: String = "D"

  val IMGP_CHARACTER_MUSHROOM: String = "mushroom"

  val IMGP_CHARACTER_TURTLE: String = "turtle"

  val IMGP_CHARACTER_MARIO: String = "mario"

  private val IMGP_OBJECT_BLOCK: String = "block"

  private val IMGP_OBJECT_MISTERY_BLOCK: String = "misteryBlock"

  private val IMGP_MUSHROOM: String = "mushroomP"

  private val IMGP_OBJECT_PIECE1: String = "piece1"

  private val IMGP_OBJECT_PIECE2: String = "piece2"

  private val IMGP_OBJECT_TUNNEL: String = "tunello"

  /*---Mario---*/
  val IMG_MARIO_DEFAULT: String = IMG_BASE + IMGP_CHARACTER_MARIO + IMGP_DIRECTION_DX + IMG_EXT

  val IMG_MARIO_SUPER_SX: String = IMG_BASE + IMGP_CHARACTER_MARIO + IMGP_STATUS_SUPER + IMGP_DIRECTION_SX + IMG_EXT

  val IMG_MARIO_SUPER_DX: String = IMG_BASE + IMGP_CHARACTER_MARIO + IMGP_STATUS_SUPER + IMGP_DIRECTION_DX + IMG_EXT

  val IMG_MARIO_ACTIVE_SX: String = IMG_BASE + IMGP_CHARACTER_MARIO + IMGP_STATUS_ACTIVE + IMGP_DIRECTION_DX + IMG_EXT

  val IMG_MARIO_ACTIVE_DX: String = IMG_BASE + IMGP_CHARACTER_MARIO + IMGP_STATUS_ACTIVE + IMGP_DIRECTION_DX + IMG_EXT

  val IMG_MARIO_DEAD: String = IMG_BASE + IMGP_CHARACTER_MARIO + IMGP_STATUS_IDLE + IMG_EXT

  /*---Mushroom---*/
  val IMG_MUSHROOM_DEAD_DX: String = IMG_BASE + IMGP_CHARACTER_MUSHROOM + IMGP_STATUS_DEAD + IMGP_DIRECTION_DX + IMG_EXT

  val IMG_MUSHROOM_DEAD_SX: String = IMG_BASE + IMGP_CHARACTER_MUSHROOM + IMGP_STATUS_DEAD + IMGP_DIRECTION_SX + IMG_EXT

  /*---Turtle---*/
  private val IMG_TURTLE_IDLE: String = IMG_BASE + IMGP_CHARACTER_TURTLE + IMGP_STATUS_IDLE + IMG_EXT

  val IMG_TURTLE_DEAD: String = IMG_TURTLE_IDLE

  /*---GameObjects--*/
  val IMG_BLOCK: String = IMG_BASE + IMGP_OBJECT_BLOCK + IMG_EXT

  val IMG_MISTERY_BLOCK: String = IMG_BASE + IMGP_OBJECT_MISTERY_BLOCK + IMG_EXT

  val IMG_MUSHROOM: String = IMG_BASE + IMGP_MUSHROOM + IMG_EXT

  val IMG_PIECE1: String = IMG_BASE + IMGP_OBJECT_PIECE1 + IMG_EXT

  val IMG_PIECE2: String = IMG_BASE + IMGP_OBJECT_PIECE2 + IMG_EXT

  val IMG_TUNNEL: String = IMG_BASE + IMGP_OBJECT_TUNNEL + IMG_EXT

  /*---Background Components---*/
  val IMG_BACKGROUND: String = IMG_BASE + "background" + IMG_EXT

  val IMG_CASTLE: String = IMG_BASE + "castelloIni" + IMG_EXT

  val START_ICON: String = IMG_BASE + "start" + IMG_EXT

  val IMG_CASTLE_FINAL: String = IMG_BASE + "castelloF" + IMG_EXT

  val IMG_FLAG: String = IMG_BASE + "bandiera" + IMG_EXT

  /*---Audio---*/
  val AUDIO_POWERUP: String = AUDIO_BASE + "powerUp.wav"

  val AUDIO_POWERDOWN: String = AUDIO_BASE + "powerDown.wav"

  val AUDIO_VICTORY: String = AUDIO_BASE + "victory.wav"

  val AUDIO_MONEY: String = AUDIO_BASE + "money.wav"

  val AUDIO_DEATH: String = AUDIO_BASE + "death.wav"

  val AUDIO_JUMP: String = AUDIO_BASE + "jump.wav"
}
