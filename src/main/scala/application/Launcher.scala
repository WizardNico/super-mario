package application

import controllers.SceneController

/**
  * Created by Andrea on 01/04/2017.
  */
object Launcher extends App {
  override def main(args: Array[String]): Unit = SceneController.startGame ()
}
