package view

import java.util.logging.{Level, Logger}

import controllers.SceneController
import utils.Utils

/**
  * Created by Andrea on 02/04/2017.
  */
class Refresh extends Runnable {

  private val PAUSE: Int = 3
  private val LOGGER: Logger = Utils.getLogger(classOf[Refresh].getName)

  def run(): Unit = {
    while (true) {
      SceneController.getScene.repaint()
      try Thread.sleep(PAUSE)
      catch {
        case e: InterruptedException => LOGGER.log(Level.WARNING, "Something goes wrong with the refresh process!", e)
      }
    }
  }

}
