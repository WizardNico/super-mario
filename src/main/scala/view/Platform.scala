package view

import java.awt.Graphics
import javax.swing.JPanel

import controllers.{BackgroundController, CharactersController, ComponentsController, SceneController}
import models.characters.Mario
import models.{Background, Position}
import utils.Keyboard

import scala.beans.BeanProperty

/**
  * Created by Andrea on 02/04/2017.
  */
class Platform extends JPanel {

  private val NUMBER_OF_PIECES: Int = 12
  private val NUMBER_OF_TUNNELS: Int = 9
  private val NUMBER_OF_BLOCKS: Int = 10
  private val NUMBER_OF_MISTERY_BLOCKS: Int = 1

  private var backgroundController: BackgroundController = _

  private var sceneController: SceneController = _

  private var componentsControllers: ComponentsController = _

  private var charactersController: CharactersController = _

  @BeanProperty var floorOffsetY: Int = 293

  @BeanProperty var heightLimit: Int = 0

  initializeControllers ()

  createScenario ()

  private def initializeControllers(): Unit = {
    this.backgroundController = new BackgroundController (new Background (Position (-50, 0)), new Background (Position (750, 0)), 0, -1)
    this.charactersController = new CharactersController ()
    this.componentsControllers = new ComponentsController (this.backgroundController, this.charactersController)
    this.sceneController = new SceneController (this.componentsControllers, this.charactersController)
  }

  private def createScenario(): Unit = {
    this.componentsControllers.createStaticComponents (NUMBER_OF_TUNNELS, NUMBER_OF_BLOCKS, NUMBER_OF_MISTERY_BLOCKS)
    this.componentsControllers.createPieces (NUMBER_OF_PIECES)
    this.setFocusable (true)
    this.requestFocusInWindow ()
    this.addKeyListener (new Keyboard ())
  }

  def getMario: Mario = this.charactersController.getMario

  def getMov: Int = backgroundController.mov

  def getXPos: Int = backgroundController.xPos

  def setBackground2PosX(x: Int): Unit = this.backgroundController.setBackground2PosX (x)

  def setXPos(xPos: Int): Unit = this.backgroundController.xPos = xPos

  def setMov(mov: Int): Unit = this.backgroundController.mov = mov

  def setBackground1PosX(x: Int): Unit = this.backgroundController.setBackground1PosX (x)

  override def paintComponent(g: Graphics): Unit = {
    super.paintComponent (g)
    this.sceneController.checkContacts ()
    this.backgroundController.updateBackgroundOnMovement ()
    this.componentsControllers.moveComponents ()
    this.sceneController.drawScenario (g, this.backgroundController)
    this.charactersController.controlCharacterStatus (g)
    this.sceneController.checkFinish (this.backgroundController)
  }

}
