package models.characters

import java.awt.Image
import java.util.logging.{Level, Logger}

import commands.Position
import models.characters.Mushroom._
import models.{BasicCharacter, Dimension, GameObject}
import utils.{Res, Utils}
/**
  * Created by Andrea on 01/04/2017.
  */
object Mushroom {
  def apply(position: Position): Mushroom = new Mushroom (position)
  val WIDTH: Int = 27
  val HEIGHT: Int = 30
  val PAUSE: Int = 15
}

class Mushroom(position: Position) extends BasicCharacter(position, Dimension(WIDTH, HEIGHT)) with Runnable {

  private val LOGGER: Logger = Utils.getLogger(classOf[Mushroom].getName)
  private var offsetX: Int = 1
  this.toRight = true
  this.moving = true

  this.createMushroom()

  private def createMushroom(): Unit = {
    new Thread(this).start()
  }

  override def move(): Unit = {
    this.offsetX = if (isToRight) 1 else -1
    setX(this.getX + this.offsetX)
  }

  override def run(): Unit = {
    while (true) {
      if (this.isAlive) {
        this.move()
        try Thread.sleep(PAUSE)
        catch {
          case e: InterruptedException => LOGGER.log(Level.WARNING, "Something goes wrong with Murshroom thread!", e)
        }
      }
    }
  }

  def contact(gameObject: GameObject): Unit = {
    if (this.hitAhead(gameObject) && this.isToRight) {
      this.toRight = false
      this.offsetX = -1
    } else if (this.hitBack(gameObject) && !this.isToRight) {
      this.toRight = true
      this.offsetX = 1
    }
  }

  def contact(character: BasicCharacter): Unit = {
    if (this.hitAhead(character) && this.isToRight) {
      this.toRight = false
      this.offsetX = -1
    } else if (this.hitBack(character) && !this.isToRight) {
      this.toRight = true
      this.offsetX = 1
    }
  }

  override def deadImage(): Image = Utils.getImage(if (this.isToRight) Res.IMG_MUSHROOM_DEAD_DX else Res.IMG_MUSHROOM_DEAD_SX)

}