package models.characters

import java.awt.Image
import java.util.logging.{Level, Logger}

import commands.Position
import models.characters.Turtle._
import models.{BasicCharacter, Dimension, GameObject}
import utils.{Res, Utils}

object Turtle {
  def apply(position: Position): Turtle = new Turtle (position)
  val WIDTH: Int = 43
  val HEIGHT: Int = 50
  val PAUSE: Int = 15
}

/**
  * Created by Andrea on 01/04/2017.
  */
class Turtle(position: Position) extends BasicCharacter (position, Dimension (WIDTH, HEIGHT)) with Runnable {

  private val LOGGER: Logger = Utils.getLogger (classOf [Turtle].getName)

  private var dxTurtle: Int = 1

  super.setToRight (true)

  super.setMoving (true)

  createTurtle ()

  private def createTurtle(): Unit = {
    new Thread (this).start ()
  }

  override def move(): Unit = {
    this.dxTurtle = if (isToRight) 1 else -1
    super.setX (super.getX + this.dxTurtle)
  }

  override def run(): Unit = {
    while (true) {
      if (this.isAlive) {
        this.move ()
        try Thread.sleep (PAUSE)
        catch {
          case e: InterruptedException => LOGGER.log (Level.WARNING, "Something goes wrong with Turtle thread!", e)
        }
      }
    }
  }

  def contact(gameObject: GameObject): Unit = {
    if (this.hitAhead (gameObject) && this.isToRight) {
      this.toRight = false
      this.dxTurtle = -1
    } else if (this.hitBack (gameObject) && !this.isToRight) {
      this.toRight = true
      this.dxTurtle = 1
    }
  }

  def contact(character: BasicCharacter): Unit = {
    if (this.hitAhead (character) && this.isToRight) {
      this.toRight = false
      this.dxTurtle = -1
    } else if (this.hitBack (character) && this.isToRight) {
      this.toRight = true
      this.dxTurtle = 1
    }
  }

  override def deadImage(): Image = Utils.getImage (Res.IMG_TURTLE_DEAD)

}