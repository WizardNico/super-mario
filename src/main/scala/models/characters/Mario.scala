package models.characters

import java.awt.Image

import commands.Position
import controllers.SceneController
import models.{Dimension, MainCharacters}
import models.characters.Mario._
import utils.{Audio, Res, Utils}

object Mario {
  def apply(position: Position): Mario = new Mario (position)
  val WIDTH: Int = 30
  val HEIGHT: Int = 50
  val JUMPING_LIMIT: Int = 42
}

/**
  * Created by Andrea on 01/04/2017.
  */
class Mario(position: Position) extends MainCharacters(position, Dimension(WIDTH, HEIGHT), Utils.getImage(Res.IMG_MARIO_DEFAULT)) {
  private var playedDeathSong: Boolean = _

  def doJump(): Image = {
    var str: String = if (this.isToRight) Res.IMG_MARIO_SUPER_DX else Res.IMG_MARIO_SUPER_SX

    incrementJumpExtent()

    if (this.getJumpExtent < JUMPING_LIMIT) {
      if (this.getY > SceneController.getScene.getHeightLimit) {
        setY(getY - 4)
      } else {
        setJumpExtent(JUMPING_LIMIT)
      }
    } else {
      if (this.getY + this.getHeight < SceneController.getScene.getFloorOffsetY) {
        setY(this.getY + 1)
      } else {
        str = if (this.isToRight) Res.IMG_MARIO_ACTIVE_DX else Res.IMG_MARIO_ACTIVE_SX
        this.jumping = false
        setJumpExtent(0)
      }
    }

    Utils.getImage(str)
  }

  override def deadImage(): Image = {
    if (!playedDeathSong) {
      playedDeathSong = true
      Audio.playSound(Res.AUDIO_DEATH)
    }
    Utils.getImage(Res.IMG_MARIO_DEAD)
  }
}