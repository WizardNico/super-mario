package models

import java.awt.Image

/**
  * Created by Andrea on 01/04/2017.
  */
trait Character {
  def walk(name: String, frequency: Int): Image
}
