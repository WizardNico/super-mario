package models

/**
  * Created by Andrea on 01/04/2017.
  */

import java.awt.Image

import commands.{Dimension, Position}
import controllers.SceneController
import utils.{Res, Utils}

import scala.beans.BooleanBeanProperty

class BasicCharacter(private var position: Position, private var dimension: Dimension) extends Character {

  val PROXIMITY_MARGIN: Int = 10
  val CONTACT_MARGIN: Int = 5

  @BooleanBeanProperty var toRight: Boolean = true

  protected var moving: Boolean = false

  private var counter: Int = 0

  protected var alive: Boolean = true

  def getX: Int = position.x

  def getY: Int = position.y

  private def getWidth: Int = this.dimension.width

  protected def getHeight: Int = this.dimension.height

  def isAlive: Boolean = alive

  def setAlive(alive: Boolean): Unit = {
    this.alive = alive
  }

  override def walk(name: String, frequency: Int): Image = Utils.getImage (Res.IMG_BASE + name + (
    if (!this.moving || {
      this.counter += 1
      this.counter
    } % frequency == 0) Res.IMGP_STATUS_ACTIVE
    else Res.IMGP_STATUS_NORMAL) + (if (this.toRight) Res.IMGP_DIRECTION_DX
  else Res.IMGP_DIRECTION_SX) + Res.IMG_EXT)

  def setX(x: Int): Unit = {
    this.position.x = x
  }

  def setY(y: Int): Unit = {
    this.position.y = y
  }

  def setMoving(moving: Boolean): Unit = {
    this.moving = moving
  }

  protected def deadImage(): Image = null

  def move(): Unit = {
    if (SceneController.getScene.getXPos >= 0) {
      this.position.x = this.position.x - SceneController.getScene.getMov
    }
  }

  protected def hitAhead(og: GameObject): Boolean =
    !(this.position.x + this.dimension.width < og.position.x ||
      this.position.x + this.dimension.width > og.position.x + CONTACT_MARGIN ||
      this.position.y + this.dimension.height <= og.position.y ||
      this.position.y >= og.position.y + og.dimension.height)

  protected def hitBack(og: GameObject): Boolean =
    !(this.position.x > og.position.x + og.dimension.width ||
      this.position.x + this.dimension.width < og.position.x + og.dimension.width - CONTACT_MARGIN ||
      this.position.y + this.dimension.height <= og.position.y ||
      this.position.y >= og.position.y + og.dimension.height)

  protected def hitBelow(og: GameObject): Boolean =
    !(this.position.x + this.dimension.width < og.position.x + CONTACT_MARGIN ||
      this.position.x > og.position.x + og.dimension.width - CONTACT_MARGIN ||
      this.position.y + this.dimension.height < og.position.y ||
      this.position.y + this.dimension.height > og.position.y + CONTACT_MARGIN)

  protected def hitAbove(og: GameObject): Boolean =
    !(this.position.x + this.dimension.width < og.position.x + CONTACT_MARGIN ||
      this.position.x > og.position.x + og.dimension.width - CONTACT_MARGIN ||
      this.position.y < og.position.y + og.dimension.height ||
      this.position.y > og.position.y + og.dimension.height + CONTACT_MARGIN)

  protected def hitAhead(character: BasicCharacter): Boolean =
    this.isToRight && !(this.position.x + this.dimension.width < character.getX ||
      this.position.x + this.dimension.width > character.getX + CONTACT_MARGIN ||
      this.position.y + this.dimension.height <= character.getY ||
      this.position.y >= character.getY + character.getHeight)

  protected def hitBack(character: BasicCharacter): Boolean =
    !(this.position.x > character.getX + character.getWidth ||
      this.position.x + this.dimension.width < character.getX + character.getWidth - CONTACT_MARGIN ||
      this.position.y + this.dimension.height <= character.getY ||
      this.position.y >= character.getY + character.getHeight)

  protected def hitBelow(character: BasicCharacter): Boolean =
    !(this.position.x + this.dimension.width < character.getX ||
      this.position.x > character.getX + character.getWidth ||
      this.position.y + this.dimension.height < character.getY ||
      this.position.y + this.dimension.height > character.getY)

  def isNearby(character: BasicCharacter): Boolean =
    (this.position.x > character.getX - PROXIMITY_MARGIN
      &&
      this.position.x < character.getX + character.getWidth + PROXIMITY_MARGIN) ||
      (this.position.x + this.dimension.width > character.getX - PROXIMITY_MARGIN
        &&
        this.position.x + this.dimension.width < character.getX + character.getWidth + PROXIMITY_MARGIN)

  def isNearby(obj: GameObject): Boolean =
    (this.position.x > obj.position.x - PROXIMITY_MARGIN
      &&
      this.position.x < obj.position.x + obj.dimension.width + PROXIMITY_MARGIN) ||
      (this.getX + this.dimension.width > obj.position.x - PROXIMITY_MARGIN
        &&
        this.position.x + this.dimension.width < obj.position.x + obj.dimension.width + PROXIMITY_MARGIN)

}
