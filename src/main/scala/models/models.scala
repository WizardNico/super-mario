import commands.{Dimension, Position}

/**
  * Created by Andrea on 01/04/2017.
  */
package object models {

  object Dimension {
    def apply(w: Int, h: Int): Dimension = new Dimension(w, h)

    def apply(): Dimension = new Dimension(0, 0)
  }

  object Position {
    def apply(x: Int, y: Int): Position = new Position(x, y)
  }

}
