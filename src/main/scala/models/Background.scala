package models

/**
  * Created by Andrea on 01/04/2017.
  */

import commands.Position
import utils.Res

import scala.beans.BeanProperty

class Background(position: Position) extends BackgroundComponent (position, Dimension (), Res.IMG_BACKGROUND) {

  @BeanProperty var backgroundPosX: Int = position.x
}
