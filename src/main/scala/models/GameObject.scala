package models

import java.awt.Image

import commands.{Dimension, Position}
import controllers.SceneController

import scala.beans.BeanProperty

/**
  * Created by Andrea on 01/04/2017.
  */
class GameObject protected(var position: Position, var dimension: Dimension) {

  @BeanProperty var objectImage: Image = _

  def move(): Unit = {
    if (SceneController.getScene.getXPos >= 0) {
      this.position.x = this.position.x - SceneController.getScene.getMov
    }
  }

}