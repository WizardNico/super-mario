package models.game_objects

import models.game_objects.StartCastle._
import models.{BackgroundComponent, Dimension, Position}
import utils.Res

object StartCastle {
  def apply: StartCastle = new StartCastle ()

  val X: Int = 10
  val Y: Int = 95
}

/**
  * Created by Andrea on 01/04/2017.
  */
class StartCastle extends BackgroundComponent (Position (X, Y), Dimension (), Res.IMG_CASTLE)

