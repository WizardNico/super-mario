package models.game_objects

import models.game_objects.Start._
import models.{BackgroundComponent, Dimension, Position}
import utils.Res

object Start {
  def apply: Start = new Start ()

  val X: Int = 220
  val Y: Int = 234
}

/**
  * Created by Andrea on 01/04/2017.
  */
class Start extends BackgroundComponent (Position (X, Y), Dimension (), Res.START_ICON)
