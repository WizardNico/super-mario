package models.game_objects

import commands.Position
import models.game_objects.Tunnel._
import models.{Dimension, GameObject}
import utils.{Res, Utils}

object Tunnel {
  def apply(position: Position): Tunnel = new Tunnel (position)

  val WIDTH: Int = 43
  val HEIGHT: Int = 65
}

/**
  * Created by Andrea on 01/04/2017.
  */
class Tunnel(position: Position) extends GameObject (position, Dimension (WIDTH, HEIGHT)) {
  super.setObjectImage (Utils.getImage (Res.IMG_TUNNEL))
}
