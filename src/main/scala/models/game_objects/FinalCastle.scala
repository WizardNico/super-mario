package models.game_objects

import models.game_objects.FinalCastle._
import models.{BackgroundComponent, Dimension, Position}
import utils.Res

object FinalCastle {
  def apply: FinalCastle = new FinalCastle ()

  val X: Int = 4850
  val Y: Int = 145
}

class FinalCastle extends BackgroundComponent (Position (X, Y), Dimension (), Res.IMG_CASTLE_FINAL)
