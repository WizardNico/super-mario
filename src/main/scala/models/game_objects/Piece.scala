package models.game_objects

import java.awt.Image
import java.util.logging.{Level, Logger}

import commands.Position
import models.game_objects.Piece._
import models.{Dimension, GameObject}
import utils.{Res, Utils}

object Piece {
  def apply(position: Position): Piece = new Piece (position)

  val WIDTH: Int = 30
  val HEIGHT: Int = 30
  val PAUSE: Int = 10
  val FLIP_FREQUENCY: Int = 500
}

/**
  * Created by Andrea on 01/04/2017.
  */

class Piece(position: Position) extends GameObject (position, Dimension (WIDTH, HEIGHT)) with Runnable {
  super.setObjectImage (Utils.getImage (Res.IMG_PIECE1))
  this.animatePiece ()

  private val LOGGER: Logger = Utils.getLogger (classOf [Piece].getName)
  private var counter: Int = _


  private def animatePiece(): Unit = {
    new Thread (this).start ()
  }

  override def run(): Unit = {
    while (true) {
      this.imageOnMovement ()
      try Thread.sleep (PAUSE)
      catch {
        case e: InterruptedException => LOGGER.log (Level.WARNING, "Something goes wrong with Piece thread!", e)
      }
    }
  }

  def imageOnMovement(): Image = {
    this.counter = this.counter + 1
    if (this.counter % FLIP_FREQUENCY == 0) Utils.getImage (Res.IMG_PIECE1) else Utils.getImage (Res.IMG_PIECE2)
  }
}

