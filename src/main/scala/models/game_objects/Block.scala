package models.game_objects

import commands.Position
import models.game_objects.Block._
import models.{Dimension, GameObject}
import utils.{Res, Utils}

object Block {
  def apply(position: Position): Block = new Block (position)
  val WIDTH: Int = 30
  val HEIGHT: Int = 30
}

/**
  * Created by Andrea on 01/04/2017.
  */
class Block(position: Position) extends GameObject (position, Dimension (WIDTH, HEIGHT)) {
  super.setObjectImage (Utils.getImage (Res.IMG_BLOCK))
}
