package models.game_objects

import models.game_objects.Flag._
import models.{BackgroundComponent, Dimension, Position}
import utils.Res

object Flag {
  def apply: Flag = new Flag ()

  val X: Int = 4650
  val Y: Int = 115
}

/**
  * Created by Andrea on 01/04/2017.
  */
class Flag extends BackgroundComponent (Position (X, Y), Dimension (), Res.IMG_FLAG)
