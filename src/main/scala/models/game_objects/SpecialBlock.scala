package models.game_objects

import commands.Position
import models.game_objects.Block._
import models.{Dimension, GameObject}
import utils.{Audio, Res, Utils}

object SpecialBlock {
  def apply(position: Position): SpecialBlock = new SpecialBlock (position)

  val WIDTH: Int = 30
  val HEIGHT: Int = 30
}

/**
  * Created by Andrea on 01/04/2017.
  */
class SpecialBlock(position: Position) extends GameObject (position, Dimension (WIDTH, HEIGHT)) {
  super.setObjectImage (Utils.getImage (Res.IMG_MISTERY_BLOCK))

  private var played: Boolean = false

  def isUsed: Boolean = played

  def powerUp(): Unit = {
    played = true
    playSong ()
    changeImage ()
  }

  private def playSong(): Unit = {
    Audio.playSound (Res.AUDIO_POWERUP)
  }

  private def changeImage(): Unit = {
    this.setObjectImage (Utils.getImage (Res.IMG_BLOCK))
  }
}
