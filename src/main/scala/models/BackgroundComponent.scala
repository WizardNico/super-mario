package models

import java.awt.Image
import javax.swing.ImageIcon

import commands.{Dimension, Position}

import scala.beans.BeanProperty

/**
  * Created by Andrea on 01/04/2017.
  */
class BackgroundComponent(position: Position, dimension: Dimension, imagePath: String) extends GameObject(position, dimension) {

  val imageIcon: ImageIcon = new ImageIcon(getClass.getResource(imagePath))

  @BeanProperty var image: Image = imageIcon.getImage

}
