package models

/**
  * Created by Andrea on 01/04/2017.
  */

import java.awt.Image
import java.util.concurrent.TimeUnit

import akka.actor.ActorRef
import akka.pattern.ask
import akka.util.Timeout
import commands.{Dimension, Position}
import controllers.SceneController
import models.characters.Mario
import models.game_objects.Piece

import scala.beans.BooleanBeanProperty
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}


class MainCharacters(position: Position, dimension: Dimension, private var marioImage: Image) extends BasicCharacter (position, dimension) {

  private val POWER_DOWN_COMMAND = "PowerDown"
  val MARIO_OFFSET_Y_INITIAL: Int = 243
  val FLOOR_OFFSET_Y_INITIAL: Int = 293

  @BooleanBeanProperty var jumping: Boolean = false

  protected var jumpingExtent: Int = 0

  def getJumpExtent: Int = jumpingExtent

  def getImage: Image = this.marioImage

  def setDeadImage(): Unit = {
    this.marioImage = deadImage ()
  }

  def incrementJumpExtent(): Unit = jumpingExtent += 1

  def setJumpExtent(jumpingExtent: Int): Unit = this.jumpingExtent = jumpingExtent

  private def checkHitAhead(obj: GameObject): Boolean = this.hitAhead (obj) && this.isToRight

  private def checkHitBack(obj: GameObject): Boolean = this.hitBack (obj) && !this.isToRight

  private def checkHitBelow(obj: GameObject): Boolean = this.hitBelow (obj) && this.jumping

  private def checkLateralContact(obj: GameObject): Unit = {
    if (checkHitAhead (obj) || checkHitBack (obj)) {
      SceneController.getScene.setMov (0)
      super.setMoving (false)
    }
  }

  private def checkBelowContact(obj: GameObject): Unit = {
    if (checkHitBelow (obj)) {
      SceneController.getScene.setFloorOffsetY (obj.position.y)
    } else if (!this.hitBelow (obj)) {
      SceneController.getScene.setFloorOffsetY (FLOOR_OFFSET_Y_INITIAL)
      if (!this.jumping) {
        super.setY (MARIO_OFFSET_Y_INITIAL)
      }
    }
  }

  private def checkAboveContact(obj: GameObject): Unit = {
    if (hitAbove (obj)) {
      // the new sky goes below the object
      SceneController.getScene.setHeightLimit (
        obj.position.y + obj.dimension.height)
    } else if (!this.hitAbove (obj) && !this.jumping) {
      // initial sky
      SceneController.getScene.setHeightLimit (0)
    }
  }

  def contactGameObject(gameObject: GameObject): Boolean = this.hitAbove (gameObject)

  def contactPiece(piece: Piece): Boolean = this.hitBack (piece) || this.hitAbove (piece) || this.hitAhead (piece) || this.hitBelow (piece)

  private def hitByEnemy(character: BasicCharacter): Boolean = this.hitAhead (character) || this.hitBack (character)

  private def changeMarioStatus(character: BasicCharacter, mario: Mario, marioActor: ActorRef): Unit = {
    implicit val timeout = Timeout (Duration (15, TimeUnit.MILLISECONDS))
    val future: Future[Boolean] = ask (marioActor, POWER_DOWN_COMMAND).mapTo [Boolean]
    val bonus = Await.result (future, timeout.duration)

    if (!bonus) {
      this.moving = false
      this.alive = false
    } else {
      changeEnemyStatus (character)
    }
  }

  private def killEnemy(character: BasicCharacter): Boolean = this.hitBelow (character)

  private def changeEnemyStatus(character: BasicCharacter): Unit = {
    character.setMoving (false)
    character.setAlive (false)
  }

  def contact(gameObject: GameObject): Unit = {
    checkLateralContact (gameObject)
    checkBelowContact (gameObject)
    checkAboveContact (gameObject)
  }

  def contact(enemyCharacter: BasicCharacter, mario: Mario, marioActor: ActorRef): Unit = {
    if (hitByEnemy (enemyCharacter) && enemyCharacter.isAlive) {
      changeMarioStatus (enemyCharacter, mario, marioActor)
    } else if (killEnemy (enemyCharacter)) {
      changeEnemyStatus (enemyCharacter)
    }
  }
}