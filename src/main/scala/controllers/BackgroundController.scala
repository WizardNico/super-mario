package controllers

import java.awt.Image

import models.Background

/**
  * Created by Andrea on 02/04/2017.
  */
class BackgroundController(private var imgBackground1: Background, private var imgBackground2: Background, var mov: Int, var xPos: Int) {

  private val BACKGROUND_POSITION: Int = 800

  def updateBackgroundOnMovement(): Unit = {
    if (this.xPos >= 0 && this.xPos <= 4600) {
      this.xPos = this.xPos + this.mov
      // Moving the screen to give the impression that Mario is walking
      setBackground1PosX (getBackground1PosX - this.mov)
      setBackground2PosX (getBackground2PosX - this.mov)
    }
    flip ()
  }

  private def flip(): Unit = {
    // Flipping between background1 and background2
    if (this.getBackground1PosX == -BACKGROUND_POSITION) {
      setBackground1PosX (BACKGROUND_POSITION)
    } else if (this.getBackground2PosX == -BACKGROUND_POSITION) {
      setBackground2PosX (BACKGROUND_POSITION)
    } else if (this.getBackground1PosX == BACKGROUND_POSITION) {
      setBackground1PosX (-BACKGROUND_POSITION)
    } else if (this.getBackground2PosX == BACKGROUND_POSITION) {
      setBackground2PosX (-BACKGROUND_POSITION)
    }
  }

  def getBackground1PosX: Int = this.imgBackground1.getBackgroundPosX

  def getBackground1Image: Image = this.imgBackground1.getImage

  def getBackground2Image: Image = this.imgBackground2.getImage

  def getBackground2PosX: Int = this.imgBackground2.getBackgroundPosX

  def setBackground1PosX(background1PosX: Int): Unit = this.imgBackground1.setBackgroundPosX (background1PosX)

  def setBackground2PosX(background2PosX: Int): Unit = this.imgBackground2.setBackgroundPosX (background2PosX)

}