package controllers

import commands.Position
import models.GameObject
import models.game_objects._

import scala.beans.BeanProperty
import scala.collection.mutable.ArrayBuffer
import scala.util.Random

/**
  * Created by Andrea on 03/04/2017.
  */
class ComponentsController(private var backgroundController: BackgroundController, private var charactersController: CharactersController) {

  private val SCATTERING_INDEX: Int = 100
  private val TUNNEL_Y_COORDINATE: Int = 230
  private val TUNNEL_X_UPPER_BOUND_COORDINATE: Int = 4500
  private val TUNNEL_X_LOWER_BOUND_COORDINATE: Int = 600
  private val BLOCK_X_UPPER_BOUND_COORDINATE: Int = 4300
  private val BLOCK_X_LOWER_BOUND_COORDINATE: Int = 400
  private val BLOCK_Y_UPPER_BOUND_COORDINATE: Int = 210
  private val BLOCK_Y_LOWER_BOUND_COORDINATE: Int = 180
  private val PIECE_X_UPPER_BOUND_COORDINATE: Int = 4600
  private val PIECE_X_LOWER_BOUND_COORDINATE: Int = 402
  private val PIECE_Y_UPPER_BOUND_COORDINATE: Int = 145
  private val PIECE_Y_LOWER_BOUND_COORDINATE: Int = 40

  @BeanProperty var finalCastle: FinalCastle = FinalCastle.apply

  @BeanProperty var startCastle: StartCastle = StartCastle.apply

  @BeanProperty var start: Start = Start.apply

  @BeanProperty var flag: Flag = Flag.apply

  @BeanProperty var pieces: ArrayBuffer[Piece] = ArrayBuffer [Piece]()

  private var objects: ArrayBuffer[GameObject] = ArrayBuffer [GameObject]()

  def getStaticComponents: ArrayBuffer[GameObject] = this.objects

  def moveComponents(): Unit = {
    if (this.backgroundController.xPos >= 0 && this.backgroundController.xPos <= 4600) {
      moveGameObject ()
      movePieces ()
      moveEnemies ()
    }
  }

  def createStaticComponents(tunnelNumber: Int, blockNumber: Int, specialBlockNumber: Int): Unit = {
    for (_ <- 0 until tunnelNumber) {
      this.objects += Tunnel (getNewTunnelPosition (this.objects))
    }
    for (_ <- 0 until blockNumber) {
      this.objects += Block (getNewBlockPosition (this.objects))
    }
    for (_ <- 0 until specialBlockNumber) {
      this.objects += SpecialBlock (getNewBlockPosition (this.objects))
    }
  }

  def createPieces(pieceNumber: Int): Unit = {
    for (_ <- 0 until pieceNumber) {
      this.pieces += Piece (getNewPiecePosition (this.pieces))
    }
  }

  private def getNewTunnelPosition(objects: ArrayBuffer[GameObject]): Position = {
    val position: Position = new Position ()
    position.y =TUNNEL_Y_COORDINATE

    var xCoordinate: Int = 0
    do xCoordinate = new Random ().nextInt (TUNNEL_X_UPPER_BOUND_COORDINATE - TUNNEL_X_LOWER_BOUND_COORDINATE) + TUNNEL_X_LOWER_BOUND_COORDINATE while (!checkXPosition (xCoordinate, objects))
    position.x = xCoordinate
    position
  }

  private def getNewBlockPosition(objects: ArrayBuffer[GameObject]): Position = {
    val position: Position = new Position ()
    var xCoordinate: Int = 0
    var yCoordinate: Int = 0
    do {
      xCoordinate = new Random ().nextInt (BLOCK_X_UPPER_BOUND_COORDINATE - BLOCK_X_LOWER_BOUND_COORDINATE) + BLOCK_X_LOWER_BOUND_COORDINATE
      yCoordinate = new Random ().nextInt (BLOCK_Y_UPPER_BOUND_COORDINATE - BLOCK_Y_LOWER_BOUND_COORDINATE) + BLOCK_Y_LOWER_BOUND_COORDINATE
    } while (!checkXPosition (xCoordinate, objects) && !checkYPosition (yCoordinate, objects))
    position.x = xCoordinate
    position.y = yCoordinate
    position
  }

  private def getNewPiecePosition(pieces: ArrayBuffer[Piece]): Position = {
    val position: Position = new Position ()
    var xCoordinate: Int = 0
    var yCoordinate: Int = 0
    do {
      xCoordinate = new Random ().nextInt (PIECE_X_UPPER_BOUND_COORDINATE - PIECE_X_LOWER_BOUND_COORDINATE) + PIECE_X_LOWER_BOUND_COORDINATE
      yCoordinate = new Random ().nextInt (PIECE_Y_UPPER_BOUND_COORDINATE - PIECE_Y_LOWER_BOUND_COORDINATE) + PIECE_Y_LOWER_BOUND_COORDINATE
    } while (!checkPiecesPosition (xCoordinate, yCoordinate, pieces))
    position.x = xCoordinate
    position.y = yCoordinate
    position
  }

  private def moveGameObject(): Unit = {
    for (obj <- objects) {
      obj.move ()
    }
  }

  private def movePieces(): Unit = {
    for (piece <- pieces) {
      piece.move ()
    }
  }

  private def moveEnemies(): Unit = {
    for (mushroom <- this.charactersController.getMushrooms) {
      mushroom.move ()
    }
    for (turtle <- this.charactersController.getTurtles) {
      turtle.move ()
    }
  }

  private def checkPiecesPosition(xPosition: Int, yPosition: Int, pieces: ArrayBuffer[Piece]): Boolean =
    pieces.find (piece => piece.position.x == xPosition && piece.position.y == yPosition).forall (_ => false)

  private def checkYPosition(yPosition: Int, objects: ArrayBuffer[GameObject]): Boolean =
    objects.find (coordinate => coordinate.position.y == yPosition || (Math.abs (coordinate.position.y - yPosition) < SCATTERING_INDEX)).forall (_ => false)

  private def checkXPosition(xPosition: Int, objects: ArrayBuffer[GameObject]): Boolean =
    objects.find (coordinate => coordinate.position.x == xPosition || (Math.abs (coordinate.position.x - xPosition) < SCATTERING_INDEX)).forall (_ => false)

}