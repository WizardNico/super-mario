package controllers

import java.awt.Graphics

import models.Position
import models.characters.{Mario, Mushroom, Turtle}
import utils.Res

import scala.beans.BeanProperty
import scala.collection.mutable.ArrayBuffer

/**
  * Created by Andrea on 02/04/2017.
  */
class CharactersController {

  private val MARIO_X: Int = 300
  private val MARIO_Y: Int = 245
  private val MARIO_FREQUENCY: Int = 25
  private val MUSHROOM_FREQUENCY: Int = 45
  private val TURTLE_FREQUENCY: Int = 45
  private val MUSHROOM_DEAD_OFFSET_Y: Int = 20
  private val MARIO_DEAD_OFFSET_Y: Int = 20
  private val TURTLE_DEAD_OFFSET_Y: Int = 30
  private val ENEMIES_SCATTERING_INDEX: Int = 1000
  private val TURTLES_NUMBER: Int = 3
  private val MUSHROOM_NUMBER: Int = 3

  @BeanProperty val mushrooms: ArrayBuffer[Mushroom] = createMushroom (MUSHROOM_NUMBER)

  @BeanProperty val turtles: ArrayBuffer[Turtle] = createTurtles (TURTLES_NUMBER)

  @BeanProperty val mario: Mario = createMario ()

  private def createMario(): Mario = Mario (Position (MARIO_X, MARIO_Y))

  private def createMushroom(num: Int): ArrayBuffer[Mushroom] = {
    val mushroomsList = ArrayBuffer [Mushroom]()
    for (i <- 0 until num) {
      mushroomsList += Mushroom (Position (800 + ENEMIES_SCATTERING_INDEX * i, 263))
    }
    mushroomsList
  }

  private def createTurtles(num: Int): ArrayBuffer[Turtle] = {
    val turtlesList = ArrayBuffer [Turtle]()
    for (i <- 0 until num) {
      turtlesList += Turtle (Position (950 + ENEMIES_SCATTERING_INDEX * i, 243))
    }
    turtlesList
  }

  def controlCharacterStatus(g: Graphics): Unit = {
    this.checkMarioStatus (g)
    this.checkMushroomIsAlive (g)
    this.checkTurtleIsAlive (g)
  }

  private def checkMarioStatus(g2: Graphics): Unit = {
    if (this.mario.isJumping) {
      g2.drawImage (this.mario.doJump (), this.mario.getX, this.mario.getY, null)
    } else {
      if (this.mario.isAlive) {
        g2.drawImage (this.mario.walk (Res.IMGP_CHARACTER_MARIO, MARIO_FREQUENCY), this.mario.getX, this.mario.getY, null)
      } else {
        this.mario.setDeadImage ()
        g2.drawImage (this.mario.deadImage (), this.mario.getX, this.mario.getY + MARIO_DEAD_OFFSET_Y, null)
        SceneController.stopGameByDeath ()
      }
    }
  }

  private def checkMushroomIsAlive(g2: Graphics): Unit = {
    for (mushroom <- this.mushrooms) {
      if (mushroom.isAlive) {
        g2.drawImage (mushroom.walk (Res.IMGP_CHARACTER_MUSHROOM, MUSHROOM_FREQUENCY), mushroom.getX, mushroom.getY, null)
      } else {
        g2.drawImage (mushroom.deadImage (), mushroom.getX, mushroom.getY + MUSHROOM_DEAD_OFFSET_Y, null)
      }
    }
  }

  private def checkTurtleIsAlive(g2: Graphics): Unit = {
    for (turtle <- this.turtles) {
      if (turtle.isAlive) {
        g2.drawImage (turtle.walk (Res.IMGP_CHARACTER_TURTLE, TURTLE_FREQUENCY), turtle.getX, turtle.getY, null)
      } else {
        g2.drawImage (turtle.deadImage (), turtle.getX, turtle.getY + TURTLE_DEAD_OFFSET_Y, null)
      }
    }
  }
}