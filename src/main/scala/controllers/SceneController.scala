package controllers

import java.awt.Graphics
import java.util.{Timer, TimerTask}
import javax.swing.{JFrame, WindowConstants}

import actors.MarioActor
import akka.actor.{ActorRef, ActorSystem, Props}
import controllers.SceneController._
import models.game_objects.SpecialBlock
import utils.{Audio, Res}
import view.{Platform, Refresh}

import scala.beans.BeanProperty

/**
  * Created by Andrea on 02/04/2017.
  */
object SceneController {

  private val GET_COIN_COMMAND: String = "AddCoin"
  private val POWER_UP_COMMAND: String = "PowerUp"
  private val WINDOW_WIDTH: Int = 700
  private val WINDOW_HEIGHT: Int = 360
  private val WINDOW_TITLE: String = "Scaled Mario"

  private val system = ActorSystem ("PiecesCounterActor")
  private val marioActor: ActorRef = system.actorOf (Props [MarioActor], name = "Mario")

  @BeanProperty var scene: Platform = _

  var up: Boolean = _

  def startGame(): Unit = {
    val window: JFrame = new JFrame (WINDOW_TITLE)
    window.setDefaultCloseOperation (WindowConstants.EXIT_ON_CLOSE)
    window.setSize (WINDOW_WIDTH, WINDOW_HEIGHT)
    window.setLocationRelativeTo (null)
    window.setResizable (true)
    window.setAlwaysOnTop (true)
    this.setScene (window)
  }

  private def setScene(window: JFrame): Unit = {
    scene = new Platform ()
    window.setContentPane (scene)
    window.setVisible (true)
    startTimer ()
  }

  private def startTimer(): Unit = {
    new Thread (new Refresh ()).start ()
  }

  private val timer = new Timer

  private def delay(f: () => Unit, n: Long): Unit = timer.schedule (new TimerTask () {
    def run(): Unit = f ()
  }, n)

  def stopGameByDeath(): Unit = {
    delay (() => System.exit (0), 2000L)
  }

  def stopGameByVictory(): Unit = {
    Audio.playSound (Res.AUDIO_VICTORY)
    delay (() => System.exit (0), 5000L)
  }
}

class SceneController(private var componentsController: ComponentsController, private var charactersController: CharactersController) {

  def drawScenario(g2: Graphics, backgroundController: BackgroundController): Unit = {
    drawBackground (g2, backgroundController)
    drawStartScenario (g2, backgroundController)
    drawGameObject (g2)
    drawPieces (g2)
    drawFinishScenario (g2, backgroundController)
  }

  private def drawFinishScenario(g2: Graphics, backgroundController: BackgroundController): Unit = {
    g2.drawImage (
      this.componentsController.getFlag.getImage,
      this.componentsController.getFlag.position.x - backgroundController.xPos,
      this.componentsController.getFlag.position.y,
      null
    )

    g2.drawImage (
      this.componentsController.getFinalCastle.getImage,
      this.componentsController.getFinalCastle.position.x - backgroundController.xPos,
      this.componentsController.getFinalCastle.position.y,
      null
    )
  }

  private def drawStartScenario(g2: Graphics, backgroundController: BackgroundController): Unit = {
    g2.drawImage (
      this.componentsController.getStartCastle.getImage,
      this.componentsController.getStartCastle.position.x - backgroundController.xPos,
      this.componentsController.getStartCastle.position.y,
      null
    )

    g2.drawImage (
      this.componentsController.getStart.getImage,
      this.componentsController.getStart.position.x - backgroundController.xPos,
      this.componentsController.getStart.position.y,
      null
    )
  }

  private def drawBackground(g2: Graphics, backgroundController: BackgroundController): Unit = {
    g2.drawImage (backgroundController.getBackground1Image, backgroundController.getBackground1PosX, 0, null)
    g2.drawImage (backgroundController.getBackground2Image, backgroundController.getBackground2PosX, 0, null)
  }

  private def drawGameObject(g2: Graphics): Unit = {
    for (gameObject <- this.componentsController.getStaticComponents) {
      g2.drawImage (gameObject.getObjectImage, gameObject.position.x, gameObject.position.y, null)
    }
  }

  private def drawPieces(g2: Graphics): Unit = {
    for (piece <- this.componentsController.getPieces) {
      g2.drawImage (piece.imageOnMovement (), piece.position.x, piece.position.y, null)
    }
  }

  def checkContacts(): Unit = {
    checkContactWithObjects ()
    checkContactWithMoney ()
    checkContactWithSpecialBlock ()
    checkContactBetweenCharacters ()
  }

  def checkFinish(backgroundController: BackgroundController): Unit = {
    if (backgroundController.xPos == this.componentsController.getFlag.position.x - 300) {
      SceneController.stopGameByVictory ()
    }
  }

  private def checkContactWithObjects(): Unit = {
    for (gameObject <- this.componentsController.getStaticComponents) {
      if (this.charactersController.getMario.isNearby (gameObject)) {
        this.charactersController.getMario.contact (gameObject)
      }

      for (mushroom <- this.charactersController.getMushrooms if mushroom.isNearby (gameObject)) {
        mushroom.contact (gameObject)
      }

      for (turtle <- this.charactersController.getTurtles if turtle.isNearby (gameObject)) {
        turtle.contact (gameObject)
      }
    }
  }

  private def checkContactWithSpecialBlock(): Unit = {
    val list = this.componentsController.getStaticComponents.filter (_.isInstanceOf [SpecialBlock]).toList
    val nElements = list.indices

    for (i <- nElements if this.charactersController.getMario.contactGameObject (list.lift (i).get) && !list.lift (i).get.asInstanceOf [SpecialBlock].isUsed) {
      list.lift (i).get.asInstanceOf [SpecialBlock].powerUp() 
      marioActor ! POWER_UP_COMMAND
    }
  }

  private def checkContactWithMoney(): Unit = {
    val list = this.componentsController.getPieces.toList
    val nElements = this.componentsController.getPieces.indices

    for (i <- nElements if this.charactersController.getMario.contactPiece (list.lift (i).get)) {
      Audio.playSound (Res.AUDIO_MONEY)
      this.componentsController.getPieces.remove (i)
      marioActor ! GET_COIN_COMMAND
    }
  }

  private def checkContactBetweenCharacters(): Unit = {
    for (turtle <- this.charactersController.getTurtles) {
      for (mushroom <- this.charactersController.getMushrooms) {
        if (mushroom.isNearby (turtle)) {
          mushroom.contact (turtle)
        }
        if (turtle.isNearby (mushroom)) {
          turtle.contact (mushroom)
        }
        if (this.charactersController.getMario.isNearby (mushroom)) {
          this.charactersController.getMario.contact (mushroom, this.charactersController.getMario, marioActor)
        }
      }
      if (this.charactersController.getMario.isNearby (turtle)) {
        this.charactersController.getMario.contact (turtle, this.charactersController.getMario, marioActor)
      }
    }
  }
}